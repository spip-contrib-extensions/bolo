<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/bolo.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bolo_description' => 'Ce plugin permet d’insérer très facilement du faux texte dans les articles ou dans les squelettes, en spécifiant le nombre de caractères souhaités.
On l’emploie soit directement dans les squelettes en utilisant le code <code>[(#BOLO|couper{800})]</code>, soit dans les champs #TEXTE des articles grâce au modèle <code><bolo800></code>.

La version actuelle permet seulement d’insérer du texte latin ; une future version pourrait permettre d’insérer du texte latin ou français accentué.',
	'bolo_nom' => 'Bolo',
	'bolo_slogan' => 'Outils pour créer du faux texte'
);
